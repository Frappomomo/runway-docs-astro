---
title: Getting Started
description: How to onboard onto the Runway Platform
---

## What is Runway?

**Runway** is a platform to streamline the deployment of projects outside of the main GitLab codebase. If you have a separate codebase containing a service or a web application that can be distributed as a docker container, **Runway** can help you easily deploy it to *staging* and *production* environments. It's in development right now but is already usable for simple deployment scenarios.


# Prerequisites

In order to be able to use Runway deployments, you need a project that fulfills the following requirements:



# Process

### 1. Add a service in the [inventory.json](https://gitlab.com/gitlab-com/gl-infra/platform/runway/provisioner/-/blob/main/inventory.json) in the [ provisioner ](https://gitlab.com/gitlab-com/gl-infra/platform/runway/provisioner) project (create an MR)

```json
    {
      "name": "eval-code-viewer",
      "project": "https://gitlab.com/gitlab-org/modelops/code-suggestions-model-evaluation/model-evaluator",
      "namespace": "ai-assist",
      "additional_roles": [],
      "members": ["myuser"],
      "groups": []
    }
```
| parameter name| description |
|--|--|
|name| The name of the application to deploy. It can be different from the project name|
|project| The link to the project, containing the application to deploy|
|namespace| to be added |
|additional_roles| to be added |
|members| A list of GitLab usernames who you wish to have permission to do Runway deployments |
|groups| A list of GitLab groups who you wish to have permission to do Runway deployments |



### 2. Check the generated project
Once inventory changes are merged, the following happens:

   1. It creates a deployment GitLab project under [Deployments](https://gitlab.com/gitlab-com/gl-infra/platform/runway/deployments) group and a service account
   2. Deployment details are located in the downstream pipeline in the created project. This pipeline should be triggered by the CI/CD pipeline of the project

For the next step you'll need to get the id of your service, which is the name of the project you can find in [Deployments](https://gitlab.com/gitlab-com/gl-infra/platform/runway/deployments). The format will be `{name}-{hash}`. For example, the service id of a service, created in a previous step could be `eval-code-viewer-78do0s`.



### 3. Allow CI job tokens from the deployment project to access your project

Follow the [Add a project to the job token scope allowlist](https://docs.gitlab.com/ee/ci/jobs/ci_job_token.html#add-a-project-to-the-job-token-scope-allowlist)
guide to allow the generated deployment project (from the previous step) to access your service project via CI.

### 4. Create runway configuration files in your repository

In the project that holds your code, you will need to create the following files

#### 1. `.runway/runway.yml`

The service manifest file contains all the configuration about your service for Runway to consume. The file is validated using [JSON schema](https://json-schema.org/) in CI pipelines.

To learn more about available attributes and format, refer to [documentation](https://gitlab-com.gitlab.io/gl-infra/platform/runway/runwayctl/manifest.schema.html).

#### 2. `.runway/env-staging.yml` and `.runway/env-production.yml`

Inside these two files are any environment variables you wish to be made available to the containers running in either the `staging` or `production` environments. Example contents of one of these files might be

```yaml
---
FASTAPI_API_PORT: "8080"
GITLAB_URL: "https://staging.gitlab.com"
GITLAB_API_URL: "https://staging.gitlab.com/api/v4/"
```

#### 3. Update your project's `.gitlab-ci.yml` file


   ```yaml
  stages:
    - runway

  include:
    - project: 'gitlab-com/gl-infra/platform/runway/ci-tasks'
      ref: v1.14.0 # renovate:managed
      file: 'service-project/runway.yml'
      inputs:
        runway_service_id: eval-code-viewer-78do0s
        image: "$CI_REGISTRY_IMAGE/deploy:$CI_COMMIT_SHORT_SHA"
        runway_version: v1.14.0 # please keep in sync with `ref` above
   ```
|input name | description|
|-----------|------------|
|runway_service_id| The id of a service that was created in the step #1 |
|image| Docker image to be deployed by Runway platform. Note, that the image should contain all the parameters required to start your service. Runway also requires you to build this image as part of your pipeline setup |
|runway_version| The version of runway components you are using. This value should be the same in the input as well as the ref field of the include statement|

### 5. Check the deployment

Once the code above is integrated into the main branch of your project, you can check `Pipelines` section and see if it was successfully deployed.

![Deployment pipeline](../../../assets/img/onboarding-pipeline-1.png)

If you unfold the Downstream pipeline, you'll see the variety of additional steps:
![Downstream pipeline](../../../assets/img/onboarding-pipeline-2.png)

It already has staging and production deployments in place, with an automatic promotion to production after 10 minutes. Once your first
deployment has taken place, you can look at the Environments section within your source repository to see the deployment status of your
environments as well as the links to access them.

### 6. Accessing your environment

The best place to find the links to access your environments (once they have been deployed) is through the Environments page in your service project. In there you should see two environments, `staging` and `production`. Clicking on the `Open` button will take you to the URL to access that environment.

**Note:** If you get SSL errors accessing an environment close to when it was deployed for the first time, this is an unfortunate side effect
of the provisioning process for GCP managed SSL certificates. Environments SSL configuration can take up to 20 minutes in order to successfully
work. This only after the first time an environment is deployed.

# Troubleshooting

If your deployment fails for any reason, check the deployment logs.

If the downstream pipeline failed to be executed, there's probably a permission mismatch.
At the moment, a person triggering a deployment must have developer's permissions in the [ci-tasks project](https://gitlab.com/gitlab-com/gl-infra/platform/runway/ci-tasks) and a maintainer's permissions in the generated deployment project (in case of the example in this doc, it's `https://gitlab.com/gitlab-com/gl-infra/platform/runway/deployments/eval-code-viewer-78do0s`). If permissions need to be changed, an MR to the [provisioner repo](https://gitlab.com/gitlab-com/gl-infra/platform/runway/provisioner) is needed.

Note that when forking an existing Runway service project, the `runway_service_id` needs to be updated or the forked service project will trigger a pipeline to the original service project's deployment project like in [this example](https://gitlab.com/schin1/ai-assist/-/pipelines/1064584911).

## How do I roll back the deployment?

At the moment the designed way to roll back a deployment is roll back a commit on the main branch, which will trigger a new deployment to Runway
with the reverted code.

## How do I view metrics?

By default, metrics are reported for all Runway services. To view metrics, you can use `service` filter on [Runway Service Metrics dashboard]https://dashboards.gitlab.net/d/runway-service/runway3a-runway-service-metrics?var-service=cfeick-dev-zg7kh0).

**Recommendation**: For additional features, such as custom service overview dashboard, alerts, and capacity planning, refer to [documentation](observability.md#Setup).

## How do I fix Terraform state error?

When a pipeline is triggered with insufficient permissions, failed job includes following error message:

```
Terraform has been successfully initialized!
Acquiring state lock. This may take a few moments...
╷
│ Error: Error acquiring the state lock
│
│ Error message: HTTP remote state endpoint invalid auth
│
│ Terraform acquires a state lock to protect the state from being written
│ by multiple users at the same time. Please resolve the issue above and try
│ again. For most commands, you can disable locking with the "-lock=false"
│ flag, but this is not recommended.
╵
Error: failed to execute terraform command: exit status 1
```

To fix, there are two potential options:

1. Job can be retried by member with existing permissions, or
2. Add your GitLab username to `members` attribute in [`inventory.json`](https://gitlab.com/gitlab-com/gl-infra/platform/runway/provisioner/-/blob/main/inventory.json?ref_type=heads) and retry job yourself

Example MR: https://gitlab.com/gitlab-com/gl-infra/platform/runway/provisioner/-/merge_requests/32.