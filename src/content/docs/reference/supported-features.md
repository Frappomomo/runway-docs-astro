---
title: Supported Features
description: This describes the features supported on Runway
---

The purpose of this document is to describe the current supported features of Ruwnay. If you have any feedback, please leave it in our [Runway Feedback Issue](https://gitlab.com/gitlab-com/gl-infra/platform/runway/team/-/issues/150)

## Supported Features

The table below describes the currently supported features on the Runway platform. 

N.B. A features absence here does not indicated that it will never be included as part of Runway. If you'd like to see a specific use case supported, please reach out to the runway team either in the [`#f_runway`](https://gitlab.slack.com/archives/C05G970PHSA) slack channel or create an issue in the [runway issue tracker](https://gitlab.com/groups/gitlab-com/gl-infra/platform/runway/-/issues).

| Feature | Support |
|---------|---------|
| Container | ✅ |
| Observability | ✅ |
| SQL Database | `Planned` |
| Object Storage | ❌ |
| NoSQL/Document DB | ❌ |
| Secrets Management| ✅| 
| Cache/MemoryStore | `Planned` |


### Observability

| Observability Feature | Configuration Required | Note |
|-----------------------|------------------------|------|
| Metrics | No | Automatically scrapes Stackdriver metrics for services. [Example](https://thanos.gitlab.net/graph?g0.expr=sum%20by\(service_name\)\(stackdriver_cloud_run_revision_run_googleapis_com_request_count%7Bjob%3D%22runway-exporter%22%2Cenv%3D%22gprd%22%7D\)&g0.tab=1&g0.stacked=0&g0.range_input=1h&g0.max_source_resolution=0s&g0.deduplicate=1&g0.partial_response=0&g0.store_matches=%5B%5D). |
| Dashboards | No | Automatically visualize and filter Stackdriver metrics by service name. [Example](https://dashboards.gitlab.net/d/runway-service/runway3a-runway-service-metrics?orgId=1&var-PROMETHEUS_DS=PA258B30F88C30650&var-environment=gprd&var-service=cfeick-dev-zg7kh0). |
| Alerts | Yes | Must have service catalog entry. [Example](https://gitlab.com/gitlab-com/gl-infra/platform/runway/docs/-/blob/master/observability.md?ref_type=heads#setup). |